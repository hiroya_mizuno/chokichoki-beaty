class AddStylistnameToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :stylistname, :string
  end
end
