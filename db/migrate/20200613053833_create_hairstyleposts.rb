class CreateHairstyleposts < ActiveRecord::Migration[5.2]
  def change
    create_table :hairstyleposts do |t|
      t.text :title
      t.text :description
      t.string :picture
      t.references :hairstylist, foreign_key: true

      t.timestamps
    end
    add_index :hairstyleposts, [:hairstylist_id, :created_at]
  end
end
