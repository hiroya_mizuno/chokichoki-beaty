class AddSelfintroductionToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :selfintroduction, :text
  end
end
