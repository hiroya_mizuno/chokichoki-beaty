class AddExperienceToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :experience, :integer
  end
end
