class AddYoutubeToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :youtube, :string
  end
end
