class CreateHairstylepictures < ActiveRecord::Migration[5.2]
  def change
    create_table :hairstylepictures do |t|
      t.references :hairstylepost, foreign_key: true

      t.timestamps
    end
  end
end
