class AddPictureurlToHairstylepicture < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylepictures, :picture_url, :string
  end
end
