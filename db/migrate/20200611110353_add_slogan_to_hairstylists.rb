class AddSloganToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :slogan, :text
  end
end
