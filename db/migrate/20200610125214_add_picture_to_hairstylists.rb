class AddPictureToHairstylists < ActiveRecord::Migration[5.2]
  def change
    add_column :hairstylists, :picture, :string
  end
end
