class HairstylepostsController < ApplicationController
  before_action :authenticate_hairstylist!, only: [:create, :update, :destroy]
  before_action :correct_hairstylist, only: [:edit, :update, :destroy]

  def index
    if params[:search].present?
      @hairstyleposts = Hairstylepost.paginate(page: params[:page]).tagged_with( params[:search], :any => true )
      if @hairstyleposts.blank?
        flash[:alert] = "該当するヘアスタイルがありません"
        @hairstyleposts = Hairstylepost.all
      end
    else
      @hairstyleposts = Hairstylepost.all
    end
  end

  def show
    @hairstylepost = Hairstylepost.find(params[:id])
  end

  def new
    @hairstylepost = Hairstylepost.new
    @hairstylepictures = @hairstylepost.hairstylepictures.build
  end

  def edit
    @hairstylepost = Hairstylepost.find(params[:id])
  end

  def create
    @hairstylepost = current_hairstylist.hairstyleposts.build(hairstylepost_params)
    if @hairstylepost.save
      flash[:success] = "hairstylepost created!"
      redirect_to hairstylist_path(current_hairstylist)
    else
      render 'top_pages/home'
    end
  end

  def update
    @hairstylepost = Hairstylepost.find_by(id: params[:id])
    #  @hairstylepost.title = params[:title]
    if @hairstylepost.update_attributes(hairstylepost_update_params)
    #  if @hairstylepost.save
      flash[:success] = "hairstylepost updated!"
      redirect_to hairstylist_path(current_hairstylist)
    else
      render 'top_pages/home'
    end
  end

  def destroy
    @hairstylepost.destroy
    flash[:success] = "Hairstylepost deleted"
    #  redirect_to request.referrer || root_url
    redirect_to hairstylist_path(current_hairstylist)
  end

  private

    def hairstylepost_params
      params.require(:hairstylepost).permit(:picture, :title, :description, tag_list:[],
      hairstylepictures_attributes: [:picture_url])
    end

    def hairstylepost_update_params
      params.require(:hairstylepost).permit(:picture, :title, :description, tag_list:[],
      hairstylepictures_attributes: [:picture_url, :_destroy, :id])
    end

    def correct_hairstylist
      @hairstylepost = current_hairstylist.hairstyleposts.find_by(id: params[:id])
      redirect_to root_url if @hairstylepost.nil?
    end
end
