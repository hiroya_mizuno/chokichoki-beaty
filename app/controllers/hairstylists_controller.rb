class HairstylistsController < ApplicationController
  def show
    @hairstylist = Hairstylist.find(params[:id])
    @hairstyleposts = @hairstylist.hairstyleposts.includes(:hairstylepictures).paginate(page: params[:page])
  end

  def edit
    @hairstylist = Hairstylist.find(params[:id])
    #  @hairstylepost = current_hairstylist.hairstyleposts.build if hairstylist_signed_in?
  end

  def update
    @hairstylist = Hairstylist.find(params[:id])
    if @hairstylist.update_attributes(hairstylist_params)
      sign_in(@hairstylist, :bypass=>true) #ヘアスタイリスト情報更新時deviseの自動ログアウトへ対応
      flash[:success] = "Profile updated"
      redirect_to @hairstylist
    else
      render 'edit'
    end
  end

    private

    def hairstylist_params
      params.require(:hairstylist).permit(:stylistname, :email, :password,
                                   :password_confirmation, :picture,
                                   :experience, :slogan, :selfintroduction, :youtube, :Line)
    end
end
