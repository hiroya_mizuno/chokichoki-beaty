module ApplicationHelper
  def full_title(page_title)
    base_title = "ChokiChoki Beauty"
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def current_hairstylist?(hairstylist)
    hairstylist == current_hairstylist
  end
end
