class Hairstylepost < ApplicationRecord
  acts_as_taggable
  belongs_to :hairstylist
  has_many :hairstylepictures, dependent: :destroy
  accepts_nested_attributes_for :hairstylepictures, allow_destroy: true
  default_scope -> { order(created_at: :desc) }
  validates :hairstylist_id, presence: true
  validates :title, presence: true
  validates :description, presence: true
  #validates :picture, presence: true
  #mount_uploader :picture, PictureUploader

  def self.search(search)
    return Hairstylepost.all unless search
    #Hairstylepost.where(['content LIKE ?', "%#{search}%"]) #検索とcontentの部分一致を表示
    Hairstylepost.tagged_with("#{search}") #acts_as_taggable_on gem で検索で使えるtagged_withメソッド
  end
end
