class Hairstylepicture < ApplicationRecord
  belongs_to :hairstylepost, optional: true
  mount_uploader :picture_url, PictureUploader
end
