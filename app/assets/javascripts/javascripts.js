$(document).on('turbolinks:load', function(){
  $(function(){

    //プレビューのhtmlを定義
    function buildHTML(count) {
      var html = `<div class="preview-box" id="preview-box__${count}">
                    <div class="upper-box">
                      <img src="" alt="preview">
                    </div>
                    <div class="lower-box">
                      <!--
                      <div class="update-box">
                        <label class="edit_btn">編集</label>
                      </div>
                      -->
                      <div class="delete-box" id="delete_btn_${count}">
                        <span>削除</span>
                      </div>
                    </div>
                  </div>`
      return html;
    }

    //削除スタック定義
    var delStack = [];

    // 投稿編集時
    //hairstyleposts/:id/editページへリンクした際のアクション=======================================
    if (window.location.href.match(/\/hairstyleposts\/\d+\/edit/)){
      //登録済み画像のプレビュー表示欄の要素を取得する
      var prevContent = $('.label-content').prev();
      labelWidth = (620 - $(prevContent).css('width').replace(/[^0-9]/g, ''));
      $('.label-content').css('width', labelWidth);
      //プレビューにidを追加　eachは.each( function(index, Element) )でマッチした各要素に対して繰り返し処理で関数を実行
      $('.preview-box').each(function(index, box){
        $(box).attr('id', `preview-box__${index}`);
      })
      //削除ボタンにidを追加
      $('.delete-box').each(function(index, box){
        $(box).attr('id', `delete_btn_${index}`);
      })
      var count = $('.preview-box').length;
      //プレビューが5あるときは、投稿ボックスを消しておく
      if (count == 5) {
        $('.label-content').hide();
      }
    }
    //=============================================================================

    // ラベルのwidth操作
    function setLabel() {
      //プレビューボックスのwidthを取得し、maxから引くことでラベルのwidthを決定
      var prevContent = $('.label-content').prev();
      labelWidth = (620 - $(prevContent).css('width').replace(/[^0-9]/g, ''));
      $('.label-content').css('width', labelWidth);
    }

    // プレビューの追加
    $(document).on('change', '.hidden-field', function() {
      setLabel();
      //hidden-fieldのidの数値のみ取得
      var id = $(this).attr('id').replace(/[^0-9]/g, '');
      //labelボックスのidとforを更新
      $('.label-box').attr({id: `label-box--${id}`,for: `hairstylepost_hairstylepictures_attributes_${id}_picture_url`});
      //選択したfileのオブジェクトを取得
      var file = this.files[0];
      var reader = new FileReader();
      //readAsDataURLで指定したFileオブジェクトを読み込む
      reader.readAsDataURL(file);
      //読み込み時に発火するイベント
      reader.onload = function() {
        var picture_url = this.result;
        //プレビューが元々なかった場合はhtmlを追加
        if ($(`#preview-box__${id}`).length == 0) {
          var count = $('.preview-box').length;
          var html = buildHTML(id);
          //ラベルの直前のプレビュー群にプレビューを追加
          var prevContent = $('.label-content').prev();
          $(prevContent).append(html);
        }
        //イメージを追加
        $(`#preview-box__${id} img`).attr('src', `${picture_url}`);
        var count = $('.preview-box').length;
        //プレビューが5個あったらラベルを隠す 
        if (count == 5) { 
          $('.label-content').hide();
        }

        //プレビュー削除したフィールドにdestroy用のチェックボックスがあった場合、チェックを外す=============
        if ($(`#hairstylepost_hairstylepictures_attributes_${id}__destroy`)){
          $(`#hairstylepost_hairstylepictures_attributes_${id}__destroy`).prop('checked',false);
        } 
        //=============================================================================

        //ラベルのwidth操作
        setLabel();
        //ラベルのidとforの値を変更
        //削除スタックに中身があるかどうか判定し、ある時は削除スタックの最後にある要素を取り出してidとforを更新
        //if(delStack.length == 0)ではなくif(delStack.length < 2)なのは１枚しか画像がない投稿を編集する時、削除ボタン→削除スタックに０格納→id=0で更新→新たに画像をid=0に追加→削除スタックの0を取り出し=>となるとid=0かつpop1=0で再度popだが削除スタックが空になっているのでundefined
        if(count < 5){
          if(delStack.length < 2){
            $('.label-box').attr({id: `label-box--${count}`,for: `hairstylepost_hairstylepictures_attributes_${count}_picture_url`});
          } else {
            //削除スタックから最後の要素を取り出す
            var pop1 = delStack.pop();
            //「削除」ボタン押したときのidとforの更新はidで行ったので、連続削除してから始めて追加の時は削除スタックの末尾要素と更新時のidが同値になるのでもう一回popする。一方それ以降追加時は削除スタック末尾要素で更新でオーケー
            if(id !== pop1){
              $('.label-box').attr({id: `label-box--${pop1}`,for: `hairstylepost_hairstylepictures_attributes_${pop1}_picture_url`});
            } else {
              var pop2 = delStack.pop();
              $('.label-box').attr({id: `label-box--${pop2}`,for: `hairstylepost_hairstylepictures_attributes_${pop2}_picture_url`});
            }
          }
        }
      }
    });

    // 画像の削除
    $(document).on('click', '.delete-box', function() {
      var count = $('.preview-box').length;
      setLabel(count);
      var id = $(this).attr('id').replace(/[^0-9]/g, '');
      $(`#preview-box__${id}`).remove();

      //削除スタックにidを入れる
      delStack.push(id);

      //新規登録時と編集時の場合分け==========================================================

      //新規投稿時
      //削除用チェックボックスの有無で判定
      if ($(`#hairstylepost_hairstylepictures_attributes_${id}__destroy`).length == 0) {
        //フォームの中身を削除 
        $(`#hairstylepost_hairstylepictures_attributes_${id}_picture_url`).val("");
        var count = $('.preview-box').length;
        //5個めが消されたらラベルを表示
        if (count == 4) {
          $('.label-content').show();
        }
        setLabel(count);
        if(id < 5){
          //削除スタックから最後の要素を取り出すのではなく、ここではidで更新する=>popは配列の要素の末尾から要素を削除して、その要素を返すメソッドなのでスタックから消してしまう。ここでは削除スタックに削除idの履歴を記録しておきたい。
          $('.label-box').attr({id: `label-box--${id}`,for: `hairstylepost_hairstylepictures_attributes_${id}_picture_url`});
        }
      } else {

        //投稿編集時
        $(`#hairstylepost_hairstylepictures_attributes_${id}__destroy`).prop('checked',true);
        if($(`#hairstylepost_hairstylepictures_attributes_${id}_picture_url`).length > 0) {
          $(`#hairstylepost_hairstylepictures_attributes_${id}_picture_url`).val("");
        }
        //5個めが消されたらラベルを表示
        if (count == 4) {
          $('.label-content').show();
        }

        //ラベルのwidth操作
        setLabel();
        //ラベルのidとforの値を変更
        //削除したプレビューのidによって、ラベルのidを変更する
        if(id < 5){
          $('.label-box').attr({id: `label-box--${id}`,for: `hairstylepost_hairstylepictures_attributes_${id}_picture_url`});
        }
      }
      //=============================================================================
    });
  });
});