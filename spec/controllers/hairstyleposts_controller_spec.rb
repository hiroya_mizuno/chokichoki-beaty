require 'rails_helper'

RSpec.describe HairstylepostsController, type: :controller do
  describe "#create" do
    context "ログイン中のヘアスタイリストではない場合" do
      it "createアクションのレスポンスのステータスが302であることを確認" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost)
        post :create, params: { hairstylepost: hairstylepost_params }
        expect(response). to have_http_status "302"
      end

      it "ヘアスタイリストサインイン画面にリダイレクトすること" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost)
        post :create, params: { hairstylepost: hairstylepost_params }
        expect(response). to redirect_to "/hairstylists/sign_in"
      end
    end
  end

  describe "#update" do
    context "別のヘアスタイリストのhairstylepostを更新しようとした場合" do
      before do
        @hairstylist = FactoryBot.create(:hairstylist)
        other_hairstylist = FactoryBot.create(:hairstylist)
        @hairstylepost = FactoryBot.create(:hairstylepost,
          hairstylist_id: other_hairstylist.id,
          title: "Old Name")
      end

      it "hairstylepostを更新できないこと" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost,
          title: "New Name")
        sign_in @hairstylist
        patch :update, params: { id: @hairstylepost.id, hairstylepost: hairstylepost_params }
        expect(@hairstylepost.reload.title).to eq "Old Name"
      end

      it "ルートページへリダイレクトすること" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost)
        sign_in @hairstylist
        patch :update, params: { id: @hairstylepost.id, hairstylepost: hairstylepost_params }
        expect(response).to redirect_to root_path
      end
    end

    context "ヘアスタイリストとしてログインしていないユーザーの場合" do
      before do
        hairstylist = FactoryBot.create(:hairstylist)
        @hairstylepost = FactoryBot.create(:hairstylepost, hairstylist_id: hairstylist.id)
      end

      it "302レスポンスを返すこと" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost)
        patch :update, params: { id: @hairstylepost.id, hairstylepost: hairstylepost_params }
        expect(response).to have_http_status "302"
      end

      it "ヘアスタイリストのサインイン画面にリダイレクトすること" do
        hairstylepost_params = FactoryBot.attributes_for(:hairstylepost)
        patch :update, params: { id: @hairstylepost.id, hairstylepost: hairstylepost_params }
        expect(response). to redirect_to "/hairstylists/sign_in"
      end
    end
  end
end
