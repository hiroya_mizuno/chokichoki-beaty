FactoryBot.define do
  factory :hairstylist do
    #association :hairstylepost
    #hairstyleposts {[
    #  FactoryBot.build(:hairstylepost, hairstylist: nil)
    #]}
    #email "testhairstylist@example.com"
    sequence(:email) {|n| "testhairstylist#{n}@example.com"}
    password "password"
  end
end
