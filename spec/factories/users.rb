FactoryBot.define do
  factory :user do
    username "user"
    email "testuser@example.com"
    password "password"
  end
end
