FactoryBot.define do
  factory :hairstylepicture do
    association :hairstylepost
    #hairstylepost
    picture_url { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/factories/test.jpg')) }
  end
end
