FactoryBot.define do
  factory :hairstylepost do
    association :hairstylist
    #hairstylist
    #hairstylepictures{[
    #  FactoryBot.build(:hairstylepicture, hairstylepost: nil)
    #]}
    sequence(:hairstylist_id) {|n| "#{n}"}
    sequence(:title) {|n| "A hairstylepost #{n}"}
    description "This is a hairstylepost"
  end
end
