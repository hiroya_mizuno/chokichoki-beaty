require 'rails_helper'

RSpec.feature "Hairstylists", type: :feature do
  it "ヘアスタイリストは自分のヘアスタイリスト登録情報を編集する" do
    hairstylist = FactoryBot.create(:hairstylist)

    visit root_path
    click_link "スタイリストログイン"
    fill_in "Email", with: hairstylist.email
    fill_in "Password", with: hairstylist.password
    click_button "Log in"

    find('.dropdown-toggle').click
    click_link "スタイリスト情報を編集する"
    
    fill_in "hairstylist[stylistname]", with: "Sample stlylist1"
    fill_in "hairstylist[email]", with: hairstylist.email
    fill_in "hairstylist[password]", with: "new-password"
    fill_in "hairstylist[password_confirmation]", with: "new-password"
    attach_file "hairstylist[picture]", "spec/factories/test.jpg"
    fill_in "hairstylist[experience]", with: "3"
    fill_in "hairstylist[slogan]", with: "Hello"
    fill_in "hairstylist[youtube]", with: "YouTbe_stylist1"
    fill_in "hairstylist[Line]", with: "line_stylist1"
    click_button "commit"

    expect(page).to have_content "Profile updated"
    expect(page).to have_content "スタイリスト情報"
  end
end
