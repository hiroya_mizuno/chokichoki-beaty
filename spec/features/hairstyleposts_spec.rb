require 'rails_helper'

RSpec.feature "Hairstyleposts", type: :feature do
  it "ヘアスタイリストは新しいヘアスタイルポストを作成する" do
    hairstylist = FactoryBot.create(:hairstylist)

    visit root_path
    click_link "スタイリストログイン"
    fill_in "Email", with: hairstylist.email
    fill_in "Password", with: hairstylist.password
    click_button "Log in"

    expect {
      click_link "新しいヘアスタイルを投稿する"
      attach_file 'hairstylepost_hairstylepictures_attributes_0_picture_url', "spec/factories/test.jpg"
      fill_in "hairstylepost[title]", with: "New Test Post"
      fill_in "hairstylepost[description]", with: "This is a New Test Post"
      click_button "投稿する"

      expect(page).to have_content "hairstylepost created!"

  }.to change(hairstylist.hairstyleposts, :count).by(1)
  end
end
