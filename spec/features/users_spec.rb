require 'rails_helper'

RSpec.feature "Users", type: :feature do
  it "ユーザーは自分のユーザー登録情報を編集する" do
    user = FactoryBot.create(:user)

    visit root_path
    click_link "ユーザーログイン"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"

    find('.dropdown-toggle').click
    click_link "ユーザー情報詳細設定"

    
    fill_in "Email", with: user.email
    fill_in "Password", with: "new-password"
    fill_in "Password confirmation", with: "new-password"
    fill_in "Current password", with: user.password
    click_button "更新する"

    expect(page).to have_content "Your account has been updated successfully."
  end
end
