require 'rails_helper'

RSpec.describe Hairstylepicture, type: :model do

  before do
    hairstylist = FactoryBot.create(:hairstylist)
    hairstylepost = Hairstylepost.create(
      hairstylist_id: hairstylist.id,
      title: "test hairstylepicture",
      description: "test hairstylepicture"
    )
    @hairstylepicture = Hairstylepicture.new(
      picture_url: Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/factories/test.jpg')),
      hairstylepost_id: hairstylepost.id
    )
  end

  it 'pictre_urlに画像のURLが入っていれば有効' do
    expect(@hairstylepicture).to be_valid
  end

end
