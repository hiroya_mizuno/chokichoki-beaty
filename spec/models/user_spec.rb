require 'rails_helper'

RSpec.describe User, type: :model do
  it "ユーザー名、メールアドレス、パスワードがあれば有効であること" do
    user = User.new(
      username: "testuser",
      email: "testuser@example.com",
      password: "password",
    )
    expect(user).to be_valid
  end

  # ユーザー名がなければ無効な状態であること 
  it "is invalid without a username" do
    user = FactoryBot.build(:user, username: nil)
    user.valid?
    expect(user.errors[:username]).to include("can't be blank")
  end

  it "ユーザー名が５１文字以上の場合は登録できない" do
    user = FactoryBot.build(:user, username: "a" * 51)
    expect(user).to be_invalid
  end

  it "ユーザー名が５０字の場合は登録できる" do
    user = FactoryBot.build(:user, username: "a" * 50)
    expect(user).to be_valid
  end

  #メールアドレスがなければ無効な状態であること
  it "is invalid without an email" do
    user = FactoryBot.build(:user, email: nil)
    user.valid?
    expect(user.errors[:email]).to include("can't be blank")
  end

  #パスワードがなければ無効な状態であること
  it "is invalid without an password" do
    user = FactoryBot.build(:user, password: nil)
    user.valid?
    expect(user.errors[:password]).to include("can't be blank")
  end 
end
