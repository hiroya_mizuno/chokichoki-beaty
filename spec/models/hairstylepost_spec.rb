require 'rails_helper'

RSpec.describe Hairstylepost, type: :model do
  it "hairstylist_id、タイトル、説明があれば有効であること" do
    hairstylist = FactoryBot.create(:hairstylist)
    #hairstylepicture = FactoryBot.create(:hairstylepicture)
    hairstylepost = FactoryBot.create(:hairstylepost, hairstylist_id: hairstylist.id)
    @hairstylepost = FactoryBot.build(:hairstylepost,
      hairstylist_id: hairstylepost.hairstylist.id,
      title: "title",
      description: "test",
    )
    expect(@hairstylepost).to be_valid
  end

  it "2つの投稿のうち最新の投稿が上位に来る" do
    hairstylist = FactoryBot.create(:hairstylist)
    # hairstylepost = FactoryBot.create(:hairstylepost, hairstylist_id: hairstylist.id)
    older_post = FactoryBot.create(:hairstylepost, hairstylist_id: hairstylist.id, created_at: 1.day.ago)
    newer_post = FactoryBot.create(:hairstylepost, hairstylist_id: hairstylist.id, created_at: 1.hour.ago)

    array = Hairstylepost.all
    expect(array).to match [newer_post, older_post]
    # Hairstylepost.all.should == [@newer_post, @older_post]
  end

  describe '#search' do

    before do
      @hairstylist = Hairstylist.create(
        email: "testtest@example.com",
        password: "password"
      )
      @winter_post = Hairstylepost.create(
        hairstylist_id: @hairstylist.id,
        title: "winter",
        description: "winter",
        tag_list: "winter"
      )
      @summer_post = Hairstylepost.create(
        hairstylist_id: @hairstylist.id,
        title: "summer",
        description: "summer",
        tag_list: "summer"
      ) 
    end

    context "「winter」で検索した場合" do
      it "winter_postを返す" do
        expect(Hairstylepost.search("winter")).to include(@winter_post)
      end
      it "summer_postを返さない" do
        expect(Hairstylepost.search("winter")).to_not include(@summer_post)
      end
    end
  end
end
