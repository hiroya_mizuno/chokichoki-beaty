Rails.application.routes.draw do
  devise_for :hairstylists, controllers: {
    sessions: 'hairstylists/sessions',
    passwords: 'hairstylists/passwords',
    registrations: 'hairstylists/registrations'
  }
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    registrations: 'users/registrations'
  }
  root 'top_pages#home'
  resources :users
  resources :hairstylists
  scope :hairstylists do
    resources :hairstyleposts, only: [:show, :destroy, :update]
  end
  resources :hairstyleposts
  get 'tags/:tag', to: 'hairstyleoposts#index', as: :tag
end
